<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230526215527 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE flight (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, guid CHAR(36) NOT NULL --(DC2Type:guid)
        , number VARCHAR(255) NOT NULL, source VARCHAR(255) NOT NULL, destination VARCHAR(255) NOT NULL, departure_date DATE NOT NULL --(DC2Type:date_immutable)
        , departure_time TIME NOT NULL --(DC2Type:time_immutable)
        , duration INTEGER NOT NULL, state VARCHAR(16) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL)');
        $this->addSql('CREATE TABLE passenger (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, passport VARCHAR(64) NOT NULL, email VARCHAR(128) NOT NULL, guid CHAR(36) NOT NULL --(DC2Type:guid)
        , created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL)');
        $this->addSql('CREATE TABLE seat (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, flight_id INTEGER NOT NULL, number VARCHAR(255) NOT NULL, state VARCHAR(16) NOT NULL, guid CHAR(36) NOT NULL --(DC2Type:guid)
        , created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, CONSTRAINT FK_3D5C366691F478C5 FOREIGN KEY (flight_id) REFERENCES flight (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_3D5C366691F478C5 ON seat (flight_id)');
        $this->addSql('CREATE TABLE ticket (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, flight_id INTEGER NOT NULL, seat_id INTEGER NOT NULL, passenger_id INTEGER NOT NULL, code VARCHAR(16) NOT NULL, guid CHAR(36) NOT NULL --(DC2Type:guid)
        , created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, CONSTRAINT FK_97A0ADA391F478C5 FOREIGN KEY (flight_id) REFERENCES flight (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_97A0ADA3C1DAFE35 FOREIGN KEY (seat_id) REFERENCES seat (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_97A0ADA34502E565 FOREIGN KEY (passenger_id) REFERENCES passenger (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_97A0ADA391F478C5 ON ticket (flight_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_97A0ADA3C1DAFE35 ON ticket (seat_id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA34502E565 ON ticket (passenger_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE flight');
        $this->addSql('DROP TABLE passenger');
        $this->addSql('DROP TABLE seat');
        $this->addSql('DROP TABLE ticket');
    }
}
