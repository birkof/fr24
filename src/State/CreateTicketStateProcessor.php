<?php

namespace App\State;

use ApiPlatform\Exception\InvalidArgumentException;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Seat;
use App\Repository\TicketRepository;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Workflow\WorkflowInterface;

class CreateTicketStateProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly ProcessorInterface $persistProcessor,
        private readonly TicketRepository $ticketRepository,
        private readonly WorkflowInterface $seatStateMachine
    ) {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if (!$this->seatStateMachine->can($data->getSeat(), Seat::TRANSITION_USE)) {
            throw new InvalidArgumentException('Seat cannot be booked!');
        }

        // Validate if Seat is already booked by this person
        if (
            $this->ticketRepository->getPassengerSeat(
                $data->getPassenger(),
                $data->getSeat(),
            )
        ) {
            throw new InvalidArgumentException('Seat has been already booked by this passenger');
        }

        // Complete Ticket missing data
        $data
            ->setFlight($data->getSeat()->getFlight())
            ->setGuid(Uuid::v4()->toRfc4122())
            ->setCode(strtoupper(bin2hex(random_bytes(5))));

        // Mark Seat as occupied
        $this->seatStateMachine->apply($data->getSeat(), Seat::TRANSITION_USE);

        return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
    }
}
