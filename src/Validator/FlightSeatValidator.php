<?php

namespace App\Validator;

use App\Entity\Seat;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class FlightSeatValidator extends ConstraintValidator
{
    public function __construct(private readonly RequestStack $requestStack)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        /* @var FlightSeat $constraint */

        if (null === $value || '' === $value) {
            return;
        }

        /** @var Seat $seat */
        $seat = $value;

        if ($seat->getFlight()->getId() !== (int)$this->requestStack->getCurrentRequest()->attributes->get('id')) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
