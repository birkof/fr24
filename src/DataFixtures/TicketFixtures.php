<?php

namespace App\DataFixtures;

use App\Entity\Passenger;
use App\Entity\Seat;
use App\Entity\Ticket;
use Doctrine\Persistence\ObjectManager;

class TicketFixtures extends BaseFixture
{
    protected function loadData(ObjectManager $manager): void
    {
        $this->createMany(Ticket::class, 2, function (Ticket $ticket, $count) {
            /** @var Seat $seat */
            $seat = $this->getReference(sprintf('%s_%d_%s%s', Seat::class, $count, '1', 'A'));

            /** @var Passenger $passenger */
            $passenger = $this->getReference(sprintf('%s_%d', Passenger::class, $count));

            $ticket
                ->setGuid($this->faker->uuid)
                ->setCode(strtoupper(bin2hex(random_bytes(5))))
                ->setFlight($seat->getFlight())
                ->setSeat($seat)
                ->setPassenger($passenger)
            ;
        });

        $manager->flush();
    }
}
