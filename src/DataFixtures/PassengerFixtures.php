<?php

namespace App\DataFixtures;

use App\Entity\Passenger;
use Doctrine\Persistence\ObjectManager;

class PassengerFixtures extends BaseFixture
{
    protected function loadData(ObjectManager $manager): void
    {
        $this->createMany(Passenger::class, 5, function (Passenger $passenger, $count) {
            $passenger
                ->setGuid($this->faker->uuid)
                ->setFullName($this->faker->name)
                ->setPassport($this->faker->creditCardNumber)
                ->setAddress($this->faker->address)
                ->setEmail($this->faker->email)
            ;
        });

        $manager->flush();
    }
}
