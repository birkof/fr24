<?php

namespace App\DataFixtures;

use App\Entity\Flight;
use App\Entity\Seat;
use Carbon\Carbon;
use Doctrine\Persistence\ObjectManager;

class FlightFixtures extends BaseFixture
{
    protected function loadData(ObjectManager $manager): void
    {
        $this->createMany(Flight::class, 10, function (Flight $flight, $count) {

            $pastStates = [Flight::STATE_ARRIVED, Flight::STATE_CANCELLED];
            $departureDate = Carbon::parse($this->faker->dateTimeInInterval('-1 year', '+2 years'));
            $departureTime = Carbon::parse($this->faker->time());

            $flight
                ->setGuid($this->faker->uuid)
                ->setNumber(mb_substr($this->faker->iban(), 0, 6))
                ->setSource($this->faker->city)
                ->setDestination($this->faker->city)
                ->setDepartureDate($departureDate->toDateTimeImmutable())
                ->setDepartureTime($departureTime->toDateTimeImmutable())
                ->setDuration($this->faker->randomNumber(5))
            ;

            // Flights with departure date in the past should be in ARRIVED or CANCELLED state.
            if ($departureDate->lessThan(Carbon::parse(Carbon::now()))) {
                shuffle($pastStates);
                $flight->setState($pastStates[0]);
            }

            // Add seats to flight with random states
            $seatStates = [Seat::STATE_AVAILABLE, Seat::STATE_RESERVED, Seat::STATE_OCCUPIED];

            foreach (range(1, 32) as $seatRow) {
                foreach (range('A', 'F') as $seatColumn) {
                    shuffle($seatStates);

                    $seat = (new Seat())
                        ->setGuid($this->faker->uuid)
                        ->setNumber($seatRow . $seatColumn)
                        ->setState($seatStates[0]);

                    $this->addReference(sprintf('%s_%d_%s%s', Seat::class, $count, $seatRow, $seatColumn), $seat);

                    $flight->addSeat($seat);
                }
            }
        });

        $manager->flush();
    }
}
