<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use App\Repository\FlightRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

#[ORM\Entity(repositoryClass: FlightRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
    ],
    normalizationContext: ['groups' => ['flight:read']],
    denormalizationContext: ['groups' => ['flight:write']],
)]

#[ApiFilter(OrderFilter::class, properties: ['id', 'departureDate'])]
class Flight
{
    use TimestampableEntity;

    public const WORKFLOW_NAME = 'flight';
    public const STATE_SCHEDULED = 'scheduled';
    public const STATE_INFLIGHT = 'in-flight';
    public const STATE_ARRIVED = 'arrived';
    public const STATE_CANCELLED = 'cancelled';

    public const TRANSITION_DEPART = 'departed';
    public const TRANSITION_ARRIVE = 'arrive';
    public const TRANSITION_CANCEL = 'cancel';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('flight:read')]
    private ?int $id = null;

    #[ORM\Column(type: Types::GUID)]
    private ?string $guid = null;

    #[ORM\Column(length: 255)]
    #[Groups('flight:read')]
    private ?string $number = null;

    #[ORM\Column(length: 255)]
    #[Groups('flight:read')]
    #[ApiFilter(SearchFilter::class, strategy: 'exact')]
    private ?string $source = null;

    #[ORM\Column(length: 255)]
    #[Groups('flight:read')]
    #[ApiFilter(SearchFilter::class, strategy: 'exact')]
    private ?string $destination = null;

    #[ORM\Column(type: Types::DATE_IMMUTABLE)]
    #[Groups('flight:read')]
    #[ApiFilter(DateFilter::class)]
    private ?DateTimeInterface $departureDate = null;

    #[ORM\Column(type: Types::TIME_IMMUTABLE)]
    #[Groups('flight:read')]
    private ?DateTimeInterface $departureTime = null;

    #[ORM\Column]
    #[Groups('flight:read')]
    private ?int $duration = null;

    #[ORM\Column(length: 16)]
    #[Groups('flight:read')]
    #[ApiFilter(SearchFilter::class, strategy: 'exact')]
    private string $state = self::STATE_SCHEDULED;

    #[ORM\OneToMany(mappedBy: 'flight', targetEntity: Seat::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups('flight:read')]
    private Collection $seats;

    #[ORM\OneToMany(mappedBy: 'flight', targetEntity: Ticket::class)]
    private Collection $tickets;

    public function __construct()
    {
        $this->seats = new ArrayCollection();
        $this->tickets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getDestination(): ?string
    {
        return $this->destination;
    }

    public function setDestination(string $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getDepartureDate(): ?DateTimeInterface
    {
        return $this->departureDate;
    }

    public function setDepartureDate(DateTimeInterface $departureDate): self
    {
        $this->departureDate = $departureDate;

        return $this;
    }

    public function getDepartureTime(): ?DateTimeInterface
    {
        return $this->departureTime;
    }

    public function setDepartureTime(DateTimeInterface $departureTime): self
    {
        $this->departureTime = $departureTime;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * @return Collection<int, Seat>
     */
    public function getSeats(): Collection
    {
        return $this->seats;
    }

    public function addSeat(Seat $seat): self
    {
        if (!$this->seats->contains($seat)) {
            $this->seats->add($seat);
            $seat->setFlight($this);
        }

        return $this;
    }

    public function removeSeat(Seat $seat): self
    {
        if ($this->seats->removeElement($seat)) {
            // set the owning side to null (unless already changed)
            if ($seat->getFlight() === $this) {
                $seat->setFlight(null);
            }
        }

        return $this;
    }

    public function isScheduled(): bool
    {
        return $this->state == self::STATE_SCHEDULED;
    }

    public function isInFlight(): bool
    {
        return $this->state == self::STATE_INFLIGHT;
    }

    public function hasArrived(): bool
    {
        return $this->state == self::STATE_ARRIVED;
    }

    public function isCancelled(): bool
    {
        return $this->state == self::STATE_CANCELLED;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets->add($ticket);
            $ticket->setFlight($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getFlight() === $this) {
                $ticket->setFlight(null);
            }
        }

        return $this;
    }
}
