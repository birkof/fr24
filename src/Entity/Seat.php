<?php

namespace App\Entity;

use ApiPlatform\Action\NotFoundAction;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use App\Repository\SeatRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SeatRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            controller: NotFoundAction::class,
            output: false,
            read: false
        ),
    ],
    normalizationContext: ['groups' => ['seat:read']],
    denormalizationContext: ['groups' => ['seat:write']],
)]
class Seat
{
    use TimestampableEntity;

    public const WORKFLOW_NAME = 'seat';
    public const STATE_AVAILABLE = 'available';
    public const STATE_RESERVED = 'reserved';
    public const STATE_OCCUPIED = 'occupied';

    public const TRANSITION_FREE = 'free';
    public const TRANSITION_STANDBY = 'standby';
    public const TRANSITION_USE = 'use';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'seats')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Flight $flight = null;

    #[ORM\Column]
    #[Groups(['seat:read', 'flight:read', 'ticket:read'])]
    private ?string $number = null;

    #[ORM\Column(length: 16)]
    #[Groups(['flight:read', 'ticket:read'])]
    private string $state = self::STATE_AVAILABLE;

    #[ORM\Column(type: Types::GUID)]
    #[Groups('flight:read')]
    private ?string $guid = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFlight(): ?Flight
    {
        return $this->flight;
    }

    public function setFlight(?Flight $flight): self
    {
        $this->flight = $flight;

        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }
}
