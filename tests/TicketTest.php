<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Seat;
use App\Entity\Ticket;

class TicketTest extends ApiTestCase
{
    public function testTicketListing(): void
    {
        static::createClient()->request('GET', '/api/tickets');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(['@id' => '/api/tickets']);
        $this->assertJsonContains(['hydra:totalItems' => 2]);
        $this->assertMatchesResourceCollectionJsonSchema(Ticket::class);
    }

    public function testCreateNewTicket(): void
    {
        $availableSeatIri = $this->findIriBy(Seat::class, ['state' => Seat::STATE_AVAILABLE]);

        static::createClient()->request('POST', '/api/tickets', ['json' => [
            'passenger' => '/api/passengers/3',
            'seat' => $availableSeatIri,
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(['@id' => '/api/tickets/3']);
        $this->assertJsonContains(['seat' => ['@id' => $availableSeatIri]]);
    }

    public function testNewlyCreatedTicket(): void
    {
        static::createClient()->request('GET', '/api/tickets');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(['@id' => '/api/tickets']);
        $this->assertJsonContains(['hydra:totalItems' => 3]);
        $this->assertMatchesResourceCollectionJsonSchema(Ticket::class);
    }

    public function testUpdateTicketSeat(): void
    {
        $availableSeatIri = $this->findIriBy(Seat::class, ['state' => Seat::STATE_RESERVED]);

        static::createClient()->request('PUT', '/api/tickets/3', ['json' => [
            'seat' => '/api/seats/3',
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(['@id' => '/api/tickets/3']);
        $this->assertJsonContains(['seat' => ['@id' => '/api/seats/3']]);
    }
}
