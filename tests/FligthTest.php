<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Flight;

class FligthTest extends ApiTestCase
{
    public function testFlightListing(): void
    {
        static::createClient()->request('GET', '/api/flights');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(['@id' => '/api/flights']);
        $this->assertJsonContains(['hydra:totalItems' => 10]);
        $this->assertMatchesResourceCollectionJsonSchema(Flight::class);
    }

    public function testFlightListingFilterByState(): void
    {
        $response = static::createClient()->request('GET', '/api/flights?state=scheduled');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(['@id' => '/api/flights']);
        $this->assertStringNotContainsString('cancelled', $response->getContent());
        $this->assertMatchesResourceCollectionJsonSchema(Flight::class);
    }
}
