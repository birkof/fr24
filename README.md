# FlightRadar24 - Airline Reservation System


## Install, Run
Using Symfony PHP >= 8.2 and composer under Docker umbrella


Installing vendor libraries
```bash
make init
```

Start the app
To start the Symfony App you need to use the following command
```bash
make start
```

Open you preferred browser and paste the following link http://127.0.0.1:8000/api

## Testing

### Code quality checks
> make test.phpmd
> 
&&
> 
> make test.phpcs

### Application Tests

`make test.phpunit` having following output:

```shell
PHPUnit 9.6.8 by Sebastian Bergmann and contributors.

Testing
......                                                              6 / 6 (100%)

Time: 00:01.266, Memory: 48.50 MB

OK (6 tests, 28 assertions)
```

### Postman

Attached you'll find a Postman collection with all the requests exposed.