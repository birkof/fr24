# makefile
#
# This file contains the commands most used in DEV environment.
#

# Mute all `make` specific output. Comment this out to get some debug information.
.SILENT:

# make commands be run with `bash` instead of the default `sh`
SHELL='/bin/sh'

# Internal variables.
DOCKER := docker run -it --rm -v ./:/usr/src/fr24 -w /usr/src/fr24 -p 8000:8000 birkof/php8.2-cli:fr24
DOCKER_NO_OUTPUT := docker run -it --rm -v ./:/usr/src/fr24 -w /usr/src/fr24 birkof/php8.2-cli:fr24

# .DEFAULT: If the command does not exist in this makefile
.DEFAULT default: ## Display this help message
	$(MAKE) help;

help:
	@echo "Usage:"
	@echo "     make [command]"
	@echo
	@echo "Available commands:"
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z\./_\-]*: *.*## *" | awk 'BEGIN {FS = ":.*?## "}; {printf "${CYAN}%-30s${NC} %s\n", $$1, $$2}'

####### ADD MORE TARGETS BELOW THIS LINE ########
pull:   ## Force pull base Docker image
	docker pull birkof/php8.2-cli:fr24

init:   ## Initialize all vendor libraries
	${DOCKER_NO_OUTPUT} composer install -o

start:	## Spin up the app
	$(DOCKER) symfony serve

bash:	## Bash inside main container
	${DOCKER_NO_OUTPUT} bash

clear-cache:  ## Clear/Refresh the application cache
	$(DOCKER) bin/console cache:clear

test.phpmd:			    ## Run tests by PHP Mess Detector
	$(DOCKER_NO_OUTPUT) ./vendor/bin/phpmd src text phpmd.xml --exclude "migrations/**,src/Kernel.php"

test.phpunit:			## Run unit tests
	$(DOCKER_NO_OUTPUT) php bin/console --env=test doctrine:database:drop --force --quiet
	$(DOCKER_NO_OUTPUT) php bin/console --env=test doctrine:schema:create --quiet
	$(DOCKER_NO_OUTPUT) php bin/console --env=test doctrine:fixtures:load --no-interaction --quiet
	$(DOCKER_NO_OUTPUT) php bin/phpunit

test.phpcs:			    ## Run tests by PHP_CodeSniffer
	$(DOCKER_NO_OUTPUT) ./vendor/bin/phpcs --standard=./phpcs.xml --ignore=src/Kernel.php

test.phpcsfix:			## Run PHP Code Beautifier and Fixer
	$(DOCKER_NO_OUTPUT) ./vendor/bin/phpcbf --standard=./phpcs.xml --ignore=src/Kernel.php
